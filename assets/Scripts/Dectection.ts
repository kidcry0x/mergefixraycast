
import { _decorator, Component, Node, Collider, director } from 'cc';
const { ccclass, property } = _decorator;

 
@ccclass('Dectection')
export class Dectection extends Component {

    protected onLoad(): void {
        const _Collider = this.getComponent(Collider);
        _Collider.on("onCollisionEnter", (event) => {
            console.log(event);
        }, this);
    }

    
}