import { _decorator, Component, Collider, ICollisionEvent, Node, Touch, macro, systemEvent, SystemEvent, Camera, geometry, PhysicsSystem, Vec3, Vec2, EventTouch, SkeletalAnimation, Quat, BoxCollider } from 'cc';

const { ccclass, property } = _decorator;

@ccclass('Raycast')
export class Raycast extends Component {

    @property(Camera)
    mainCamera: Camera = null;

    @property([Node])
    player: Node[] = [];

    @property(Node)
    ground: Node = null;

    _ray: geometry.Ray = new geometry.Ray();
    _maxDistance = 10000;
    _mask: number = 0xffffffff;

    _pos: Vec3 = new Vec3();
    _posPlayerStart_0: Vec3 = new Vec3(-0.098, 1.484, 0);
    _posPlayerStart_1: Vec3 = new Vec3(-0.086, 1.62, 1.929);
    _posPlayerStart_2: Vec3 = new Vec3(-0.068, 1.503, 2.995);
    _posPlayerStart_3: Vec3 = new Vec3(-1.113, 1.676, 1.929);
    _posPlayerStart_4: Vec3 = new Vec3(0.942, 1.521, 1.929);

    private _player: Node = null;

    onEnable() {
        systemEvent.on(SystemEvent.EventType.TOUCH_START, this.onTouchStart, this);
        systemEvent.on(SystemEvent.EventType.TOUCH_MOVE, this.onTouchMove, this);
        systemEvent.on(SystemEvent.EventType.TOUCH_END, this.onTouchEnd, this);

        this.player.forEach(player => {
            player.getComponent(SkeletalAnimation).play("metarig|idle");
            // player.getComponent(Collider).on('onCollisionEnter', this.onCollision, this);
            // player.getComponent(Collider).on('onCollisionStay', this.onCollision, this);
            // player.getComponent(Collider).on('onCollisionExit', this.onCollision, this);
        });
        
        this.player[4].getComponent(SkeletalAnimation).play("metarig|idle_1");
        macro.ENABLE_MULTI_TOUCH = false;
    }

    onDisable() {
        systemEvent.off(SystemEvent.EventType.TOUCH_START, this.onTouchStart, this);
        systemEvent.off(SystemEvent.EventType.TOUCH_MOVE, this.onTouchMove, this);
        systemEvent.off(SystemEvent.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    onCollision(event: ICollisionEvent) {
        console.log(event.type, event);
        console.log(this.player.length);
    }

    onTouchStart(touch: Touch) {
        this.mainCamera.screenPointToRay(touch.getLocationX(), touch.getLocationY(), this._ray);
        PhysicsSystem.instance.raycast(this._ray, this._mask, this._maxDistance, true)
        const r = PhysicsSystem.instance.raycastResults;
        for (let i = 0; i < r.length; ++i)
        {   
            const Layer = r[i].collider.node.layer;
            switch (Layer) {
                case this.player[0].layer:
                    this._player = this.player[0];
                    this.player[0].getComponent(SkeletalAnimation).crossFade("metarig|fall", 0.5);
                    // this.player[0].rotate(Quat.fromEuler(new Quat(), -30, 0, 0));
                break;

                case this.player[1].layer:
                    this._player = this.player[1];
                    this.player[1].getComponent(SkeletalAnimation).crossFade("metarig|fall", 0.5);
                break;

                case this.player[2].layer:
                    this._player = this.player[2];
                    this.player[2].getComponent(SkeletalAnimation).crossFade("metarig|fall", 0.5);
                break;

                case this.player[3].layer:
                    this._player = this.player[3];
                    this.player[3].getComponent(SkeletalAnimation).crossFade("metarig|fall", 0.5);
                break;

                case this.player[4].layer:
                    this._player = this.player[4];
                    this.player[4].getComponent(SkeletalAnimation).crossFade("metarig|fall", 0.5);
                break;
            }
        }
    }

    onTouchMove(touch: Touch, event: EventTouch) {
        if (this._player)
        {
            this.mainCamera.screenPointToRay(touch.getLocationX(), touch.getLocationY(), this._ray);
            PhysicsSystem.instance.raycast(this._ray, this._mask, this._maxDistance, true)
            const r = PhysicsSystem.instance.raycastResults;
            // console.log(r)
            for (let i = 0; i < r.length; ++i) {
                if (r[i].collider.node.layer === this.ground.layer)
                {   
                    this._pos.x = r[i]["_hitPoint"].x;
                    this._pos.y = this._player.position.y;
                    this._pos.z = r[i]["_hitPoint"].z;
                    this._player.setPosition(this._pos);
                }
            }
        }
    }

    onTouchEnd(touch: Touch) {
        this._player = null;

        this.mainCamera.screenPointToRay(touch.getLocationX(), touch.getLocationY(), this._ray);
        PhysicsSystem.instance.raycast(this._ray, this._mask, this._maxDistance, true)
        const r = PhysicsSystem.instance.raycastResults;
        for (let i = 0; i < r.length; ++i)
        {   
            const Layer = r[i].collider.node.layer;
            switch (Layer) {
                case this.player[0].layer:
                    this._player = this.player[0];
                    this.player[0].getComponent(SkeletalAnimation).play("metarig|idle");
                    this.player[0].setPosition(this._posPlayerStart_0)
                    // this.player[0].rotate(Quat.fromEuler(new Quat(), 30, 0, 0));
                break;

                case this.player[1].layer:
                    this._player = this.player[1];
                    this.player[1].getComponent(SkeletalAnimation).play("metarig|idle");;
                    this.player[1].setPosition(this._posPlayerStart_1)
                break;

                case this.player[2].layer:
                    this._player = this.player[2];
                    this.player[2].getComponent(SkeletalAnimation).play("metarig|idle");
                    this.player[2].setPosition(this._posPlayerStart_2)
                break;

                case this.player[3].layer:
                    this._player = this.player[3];
                    this.player[3].getComponent(SkeletalAnimation).play("metarig|idle");
                    this.player[3].setPosition(this._posPlayerStart_3)
                break;

                case this.player[4].layer:
                    this._player = this.player[4];
                    this.player[4].getComponent(SkeletalAnimation).play("metarig|idle_1");
                    this.player[4].setPosition(this._posPlayerStart_4)
                break;
            }
        }
    }

    update (deltaTime: number) {
        if (this._player === this.player[0]) {
            
        }
    }
}
